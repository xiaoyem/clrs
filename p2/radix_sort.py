#
# Copyright (c) 2016-2018 by Yuchao Zhao.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import operator

def radix_sort(a, d):
    for i in range(d):
        p = []
        for j in range(len(a)):
            p.append(a[j] / pow(10, i) % 10)
        link = sorted(zip(p, a), key = operator.itemgetter(0))
        a[:] = map(operator.itemgetter(1), link)

