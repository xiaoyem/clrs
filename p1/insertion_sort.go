//
// Copyright (c) 2016-2018 by Qiang Meng.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

package p1

func InsertionSort(a []int) {
    n := len(a)
    for i := 1; i < n; i++ {
        j, key := i - 1, a[i]
        for j >= 0 && a[j] > key {
            a[j + 1] = a[j]
            j--
        }
        a[j + 1] = key
    }
}

// Loop invariant: at the start of each iteration of the for loop, the subarray
// a[0..i - 1] consists of the elements originally in a[0..i - 1], but in sorted
// order.

